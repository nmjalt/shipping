package javastart.team.shipping.services;

import javastart.team.shipping.model.CommodityGroup;
import javastart.team.shipping.repositories.CommodityGroupJpaRepository;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.EnumSet;
import java.util.List;

import static org.hibernate.cfg.AvailableSettings.DATASOURCE;
import static org.hibernate.cfg.AvailableSettings.DIALECT;
import static org.hibernate.tool.schema.TargetType.DATABASE;
import static org.hibernate.tool.schema.TargetType.STDOUT;

@org.springframework.stereotype.Service
public class ShippingService {

    @Qualifier("shippingDataSourcePool")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private CommodityGroupJpaRepository commodityGroupJpaRepository;

    @Transactional
    public Long createCommodityGroup(String name) {
        CommodityGroup commodityGroup = new CommodityGroup();
        commodityGroup.setName(name);
        return commodityGroupJpaRepository.save(commodityGroup).getId();
    }

    public List<CommodityGroup> findAllCommodityGroups() {
        return commodityGroupJpaRepository.findAll();
    }

    public void generateSchema() {
        StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder()
                .applySetting(DATASOURCE, dataSource)
                .applySetting(DIALECT, HSQLDialect.class);
        MetadataSources metadataSources = new MetadataSources(registryBuilder.build());

        PathMatchingResourcePatternResolver resourceLoader = new PathMatchingResourcePatternResolver();
        new LocalSessionFactoryBuilder(null, resourceLoader, metadataSources)
                .scanPackages("javastart.team.shipping.model");

        Metadata metadata = metadataSources.buildMetadata();

        new SchemaExport()
                .setFormat(true)
                .create(EnumSet.of(STDOUT, DATABASE), metadata);
    }

}
