package javastart.team.shipping.services;

import javastart.team.shipping.model.CommodityGroup;
import javastart.team.shipping.model.DischargePort;
import javastart.team.shipping.repositories.DischargePortJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@org.springframework.stereotype.Service
public class PortService {

    @Autowired
    private DischargePortJpaRepository dischargePortJpaRepository;

    //    public DischargePort findPortById(Long id) {
//
//    }
//
//    public DischargePort findPortByName(String name) {
//
//    }
//
    public Collection<CommodityGroup> findAcceptedPortGroupsById(Long id) {
        Optional<DischargePort> byId = dischargePortJpaRepository.findById(id);
        if (byId.isPresent()) {
            return byId.get().getAcceptedCommodityGroups();
        }
        return Collections.emptySet();
    }
//
//    public Collection<CommodityGroup> findAcceptedPortGroupsByName(String name) {
//
//    }



}
