package javastart.team.shipping;

import com.samskivert.mustache.Mustache;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mustache.MustacheEnvironmentCollector;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

// https://github.com/eugenp/tutorials/blob/master/mustache/src/main/java/com/baeldung/springmustache/SpringMustacheApplication.java

@ServletComponentScan
@SpringBootApplication
public class ShippingApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ShippingApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ShippingApplication.class, args);
    }

    // https://github.com/spullara/mustache.java
    // https://www.baeldung.com/spring-boot-mustache
    // https://www.baeldung.com/mustache
    @Bean
    public Mustache.Compiler mustacheCompiler(Mustache.TemplateLoader templateLoader, Environment environment) {

        MustacheEnvironmentCollector collector = new MustacheEnvironmentCollector();
        collector.setEnvironment(environment);

        return Mustache.compiler()
                .defaultValue("Some Default Value")
                .withLoader(templateLoader)
                .withCollector(collector);

    }

}
