package javastart.team.shipping.repositories;

import javastart.team.shipping.model.DischargePort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DischargePortJpaRepository extends JpaRepository<DischargePort, Long> {


}
