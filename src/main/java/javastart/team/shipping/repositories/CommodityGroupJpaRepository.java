package javastart.team.shipping.repositories;

import javastart.team.shipping.model.CommodityGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommodityGroupJpaRepository extends JpaRepository<CommodityGroup, Long> {


}
