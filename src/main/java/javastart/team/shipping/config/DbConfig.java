package javastart.team.shipping.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@Import({ShippingJpaConfig.class})
@PropertySource("classpath:application.properties")
public class DbConfig {

//    @Autowired
//    private Environment env;

    @Bean
    public DataSource shippingDataSourcePool() {
        String dbDriver = "org.hsqldb.jdbc.JDBCDriver";
        String dbURL = "jdbc:hsqldb:hsql://127.0.0.1:9921/mainDb";
        String dbUsr = "SA";
        String pass = "";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbURL);
        dataSource.setUsername(dbUsr);
        dataSource.setPassword(pass);
        return dataSource;
    }

}
