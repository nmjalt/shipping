package javastart.team.shipping.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.SharedCacheMode;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "javastart.team.shipping.repositories",
        entityManagerFactoryRef = "shippingJpaEntityManagerFactory",
        transactionManagerRef = "shippingJpaTransactionManager"
)
public class ShippingJpaConfig {

    @Qualifier("shippingJpaEntityManagerFactory")
    @Bean
    public EntityManagerFactory shippingJpaEntityManagerFactory(@Autowired @Qualifier("shippingDataSourcePool") DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        Map<String, Object> jpaProperties = new HashMap<>();
        jpaProperties.put("hibernate.jdbc.time_zone", "UTC");
        factory.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);
        factory.setPersistenceUnitName("shipping");
        factory.setJpaPropertyMap(jpaProperties);
        factory.setJpaVendorAdapter(shippingJpaVendorAdapter());
        factory.setPackagesToScan("javastart.team.shipping.model");
        factory.setDataSource(dataSource);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Qualifier("shippingJpaTransactionManager")
    @Bean
    public PlatformTransactionManager shippingJpaTransactionManager(@Autowired @Qualifier("shippingJpaEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Qualifier("shippingJpaVendorAdapter")
    @Bean
    public JpaVendorAdapter shippingJpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabase(Database.HSQL);
        jpaVendorAdapter.setShowSql(false);
        return jpaVendorAdapter;
    }

}
