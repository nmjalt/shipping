package javastart.team.shipping.web.controller;

import javastart.team.shipping.model.CommodityGroup;
import javastart.team.shipping.services.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class ControllerHtml {

    @Autowired
    private ShippingService shippingService;

    @GetMapping("/groups")
    public ModelAndView displayGroups(Map<String, Object> model) {
        List<CommodityGroup> allCommodityGroups = shippingService.findAllCommodityGroups();

        model.put("groups", allCommodityGroups);

        return new ModelAndView("groupsToDisplay", model);
    }

    @GetMapping("/newGroupForm")
    public ModelAndView newGroupForm(Map<String, Object> model) {
        return new ModelAndView("newGroup", model);
    }

}
