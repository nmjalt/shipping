package javastart.team.shipping.web.controller;


import io.swagger.annotations.ApiParam;
import javastart.team.shipping.model.CommodityGroup;
import javastart.team.shipping.services.PortService;
import javastart.team.shipping.services.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Validated
@RestController
@CrossOrigin
public class ControllerShipping {

    @Autowired
    private ShippingService shippingService;

    @Autowired
    private PortService portService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/newCommodityGroup", method = {RequestMethod.GET}, name = "REST API to create CommodityGroup")
    public String newCommodityGroup(@ApiParam(required = true, value = "name") @RequestParam(name = "name") String name) {
        shippingService.createCommodityGroup(name);
        return "groups";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/findAll", method = {RequestMethod.GET}, name = "REST API to return all commodity group's name")
    public List<CommodityGroup> findAllCommodityGroups() {
        return shippingService.findAllCommodityGroups();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/genSchema", method = {RequestMethod.GET}, name = "REST API to generate db schema")
    public void generateSchema() {
        shippingService.generateSchema();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/findByDate", method = {RequestMethod.GET}, name = "REST API to return commodity groups by date")
    public List<CommodityGroup> findByDate(
            @ApiParam(required = true, value = "date yyyy-MM-dd HH:mm:ss", format = "yyyy-MM-dd HH:mm:ss")
            @RequestParam(name = "date")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date) {
        System.out.println(date);
        return Collections.emptyList();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/findAcceptedPortGroupsById", method = {RequestMethod.GET}, name = "REST API to find accepted ports")
    public Collection<CommodityGroup> findAcceptedPortGroupsById(@ApiParam(required = true, value = "id") @RequestParam(name = "id") Long id) {
        return portService.findAcceptedPortGroupsById(id);
    }

}
