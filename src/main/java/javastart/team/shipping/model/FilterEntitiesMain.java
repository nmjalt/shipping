package javastart.team.shipping.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterEntitiesMain {
//
//    static int cargoAcceptanceIdSeq = 0;
//
//    public static void main(String[] args) {
//        List<CommodityGroup> commodityGroupList = new ArrayList<>(3);
//        commodityGroupList.add(commodityGroup(1L, "timber"));
//        commodityGroupList.add(commodityGroup(2L, "chemicals"));
//        commodityGroupList.add(commodityGroup(3L, "plastic scraps"));
//        Map<Long, CommodityGroup> commodityGroupMap = new HashMap<>(3);
//        commodityGroupList.forEach(g -> commodityGroupMap.put(g.getId(), g));
//
//        List<DischargePort> dischargePortList = new ArrayList<>(4);
//        dischargePortList.add(dischargePort(1, "Shanghai"));
//        dischargePortList.add(dischargePort(2, "New York"));
//        dischargePortList.add(dischargePort(3, "Mumbai"));
//        dischargePortList.add(dischargePort(4, "Long Beach"));
//        Map<Integer, DischargePort> dischargePortMap = new HashMap<>(3);
//        dischargePortList.forEach(g -> dischargePortMap.put(g.getId(), g));
//
//        List<CargoAcceptance> cargoAcceptanceList = new ArrayList<>(4);
//        cargoAcceptanceList.add(cargoAcceptance(true, 1, 1));
//        cargoAcceptanceList.add(cargoAcceptance(false, 1, 2));
//        cargoAcceptanceList.add(cargoAcceptance(true, 2, 3));
//        cargoAcceptanceList.add(cargoAcceptance(true, 1, 3));
//        cargoAcceptanceList.add(cargoAcceptance(false, 3, 3));
//
//        // 1. Which commodity groups are not accepted anywhere
//        commodityGroupsNotAcceptedAnywhere(commodityGroupMap, commodityGroupList, cargoAcceptanceList)
//                .forEach(commodityGroup -> System.out.println(commodityGroup));
//
//        // 2. Which ports does not have any commodity groups accepted
//        portsNotHaveAnyCGAccepted(dischargePortList, cargoAcceptanceList)
//                .forEach(ports -> System.out.println(ports));
//
//        // 3. Which ports have at least one accepted commodity group.
//        portsWithAtLeastOneAcceptedCG(dischargePortMap, dischargePortList, cargoAcceptanceList)
//                .forEach(ports -> System.out.println(ports));
//
//
//    }
//
//    private static Stream portsWithAtLeastOneAcceptedCG(Map<Integer, DischargePort> dischargePortMap, List<DischargePort> dischargePortList, List<CargoAcceptance> cargoAcceptanceList) {
//        System.out.println("\n3. Which ports have at least one accepted commodity group.");
//        Set<DischargePort> dischargePorts = new HashSet<>();
//        cargoAcceptanceList.stream()
//                .filter(cargoAcceptance -> cargoAcceptance.getAccepted() != 0)
//                .forEach(cargoAcceptance -> dischargePorts.add(dischargePortMap.get(cargoAcceptance.getIdPort())));
//        return dischargePorts.stream();
//
//    }
//
//    public static Stream portsNotHaveAnyCGAccepted(List<DischargePort> dischargePortList, List<CargoAcceptance> cargoAcceptanceList) {
//        System.out.println("\n2. Which ports does not have any commodity groups accepted");
//        return dischargePortList.stream().filter(dischargePort -> cargoAcceptanceList.stream().filter(cargoAcceptance ->
//                cargoAcceptance.getIdPort() == dischargePort.getId()  && cargoAcceptance.getAccepted() == 1)
//                .collect(Collectors.toList()).size() == 0);
//
//    }
//
//    public static Stream commodityGroupsNotAcceptedAnywhere(Map<Long, CommodityGroup> commodityGroupMap, List<CommodityGroup> commodityGroupList, List<CargoAcceptance> cargoAcceptanceList) {
//        System.out.println("\n1. Which commodity groups are not accepted anywhere");
//        Set<CommodityGroup> acceptedCG = new HashSet<>();
//
//        cargoAcceptanceList.stream().filter(cargoAcceptance -> cargoAcceptance.getAccepted() == 1)
//                .forEach(cargoAcceptance -> acceptedCG.add(commodityGroupMap.get((long)cargoAcceptance.getIdGroup())));
//
//        return commodityGroupList.stream().filter(commodityGroup -> (!acceptedCG.contains(commodityGroup)));
//
///*        for (CommodityGroup commodityGroup : commodityGroupList) {
//            boolean forbiddenInEveryPort = true;
//            for (CargoAcceptance cargoAcceptance : cargoAcceptanceList) {
//                if (((long)cargoAcceptance.getIdGroup()) == commodityGroup.getId() && cargoAcceptance.getAccepted() == 1) {
//                    forbiddenInEveryPort = false;
//                }
//            }
//            if (forbiddenInEveryPort)
//                System.out.println(commodityGroup + " forbidden everywhere");
//        }*/
//    }
//
//    private static CargoAcceptance cargoAcceptance(boolean accepted, int idGroup, int idPort) {
//        CargoAcceptance cargoAcceptance = new CargoAcceptance();
//        cargoAcceptance.setId(++cargoAcceptanceIdSeq);
//        cargoAcceptance.setAccepted(accepted ? 1 : 0);
//        cargoAcceptance.setIdGroup(idGroup);
//        cargoAcceptance.setIdPort(idPort);
//        return cargoAcceptance;
//    }
//
//    private static DischargePort dischargePort(int id, String name) {
//        DischargePort dischargePort = new DischargePort();
//        dischargePort.setId(id);
//        dischargePort.setName(name);
//        return dischargePort;
//    }
//
//    private static CommodityGroup commodityGroup(Long id, String name) {
//        CommodityGroup commodityGroup = new CommodityGroup();
//        commodityGroup.setId(id);
//        commodityGroup.setName(name);
//        return commodityGroup;
//    }

}
