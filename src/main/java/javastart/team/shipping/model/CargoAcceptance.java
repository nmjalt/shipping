package javastart.team.shipping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CargoAcceptance {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column
    private Integer idPort;

    @Column
    private Integer idGroup;

    @Column
    private Integer accepted;

    public Long getId() {
        return id;
    }

    public Integer getIdPort() {
        return idPort;
    }

    public void setIdPort(Integer idPort) {
        this.idPort = idPort;
    }

    public Integer getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(Integer idGroup) {
        this.idGroup = idGroup;
    }

    public Integer getAccepted() {
        return accepted;
    }

    public void setAccepted(Integer accepted) {
        this.accepted = accepted;
    }

    @Override
    public String toString() {
        return "CargoAcceptance{" +
                "id=" + id +
                ", idPort=" + idPort +
                ", idGroup=" + idGroup +
                ", accepted=" + accepted +
                '}';
    }

}
