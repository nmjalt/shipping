package javastart.team.shipping.model;

import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class DischargePort {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column
    private String name;

    @ManyToMany(cascade = {CascadeType.REFRESH})
    @JoinTable(
            name = "CargoAcceptance",
            joinColumns = {@JoinColumn(name = "idPort")},
            inverseJoinColumns = {@JoinColumn(name = "idGroup")}
    )
    @WhereJoinTable(clause = "accepted!=0")
    private Set<CommodityGroup> groups = new HashSet<>();

    public Set<CommodityGroup> getAcceptedCommodityGroups() {
        return groups;
    }
//
//    public void setAcceptedCommodityGroupList(List<CommodityGroup> acceptedCommodityGroupList) {
//        this.acceptedCommodityGroupList = acceptedCommodityGroupList;
//    }
//
//    public void addToCommodityGroup(CommodityGroup commodityGroup) {
//        this.acceptedCommodityGroupList.add(commodityGroup);
//    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DischargePort{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
